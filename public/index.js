const users = [
  {
    uid: 001,
    email: "john@dev.com",
    personalInfo: {
      name: "John",
      address: {
        line1: "westwish st",
        line2: "washmasher",
        city: "wallas",
        state: "WX",
      },
    },
  },
  {
    uid: 063,
    email: "a.abken@larobe.edu.au",
    personalInfo: {
      name: "amin",
      address: {
        line1: "Heidelberg",
        line2: "",
        city: "Melbourne",
        state: "VIC",
      },
    },
  },
  {
    uid: 045,
    email: "Linda.Paterson@gmail.com",
    personalInfo: {
      name: "Linda",
      address: {
        line1: "Cherry st",
        line2: "Kangaroo Point",
        city: "Brisbane",
        state: "QLD",
      },
    },
  },
];

const returnUsers = (users) => {
  const res = [];
  for (const user of users) {
    res.push({
      name: user.personalInfo.name,
      email: user.email,
      state: user.personalInfo.address.state,
    });
  }
  return res;
};

const displayUsersBtnClick = () => {
  displayUsers(returnUsers(users));
};

const displayUsers = (users) => {
  const table = document.getElementById("tbl");

  var len = table.rows.length;
  while (len-- > 0) table.deleteRow(0);

  for (const item of users) {
    const row = table.insertRow(table.rows.length);
    const name = row.insertCell(0);
    const email = row.insertCell(1);
    const state = row.insertCell(2);
    name.innerHTML = item.name;
    email.innerHTML = item.email;
    state.innerHTML = item.state;
  }
};

const sortAndDisplay = () => {
  const urs = returnUsers(users);
  urs.sort((a, b) => a.name.localeCompare(b.name));
  displayUsers(urs);
};
